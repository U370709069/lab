package exam;

public class Circle {
	
	protected double radius;
	static double pi=3.14;
	
	public Circle(double radius ) {
		
		this.radius = radius;
		
	}
	
	public double area() {
		return radius*radius*pi;
	}
	
	public double perimeter() {
		
		return (radius)*2*pi;
	}

	public String toString() {
		
		return "radius = " + radius;
	}
}
